package com.finalproject.trym.smartbuy;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddItem extends AppCompatActivity {

    //Permisos
    private Permissions permissions;

    //This variable needs to keep getting updated,
    static int fk_usuario;
    static String image_url;
    //static String image_url_Upload;

    //Capturar Imagen
    private static final String TAG = AddItem.class.getSimpleName();
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri; // file url to store image/video
    private ImageView imagePreview;
    private ProgressBar progressBar;
    private TextView txtPercentage;
    long totalSize = 0;
    String filePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        permissions = new Permissions(AddItem.this);

        final EditText etProductName = (EditText) findViewById(R.id.etProductName);
        final EditText etProductPrice = (EditText) findViewById(R.id.etProductPrice);
        final EditText etProductPriceDiscount = (EditText) findViewById(R.id.etProductPriceDiscount);
        final EditText etBrand = (EditText) findViewById(R.id.etBrand);
        final EditText etStoreName = (EditText) findViewById(R.id.etStoreName);
        final EditText etStoreLocation = (EditText) findViewById(R.id.etStoreLocation);

        final Button btnUpload = (Button) findViewById(R.id.btnUpload);

        //PARA LA IMAGEN
        //final ImageButton btnImg = (ImageButton) findViewById(R.id.btnImage);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imagePreview = (ImageView) findViewById(R.id.imgPreview);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);


        //Capturar Imagen
        final ImageButton btnImage =(ImageButton) findViewById(R.id.btnImage);

        String id_usuario;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        //System.out.println("************"+bundle.getString("id_usuario"));
        if (bundle != null){
            //String id_usuario;
            id_usuario = bundle.getString("id_usuario");
            //int fk_usuario = Integer.parseInt(id_usuario);
            fk_usuario = Integer.parseInt(id_usuario);
//            AddItemRequest addItemRequest = new AddItemRequest(productName, productPrice, productPriceDiscount, brand, storeName, storeLocation, fk_usuario, responseListener);
//            RequestQueue queue = Volley.newRequestQueue(AddItem.this);
//            queue.add(addItemRequest);
        }

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }

        });

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
            finish();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String productName = etProductName.getText().toString();
                final double productPrice = Double.parseDouble(etProductPrice.getText().toString());
                final double productPriceDiscount = Double.parseDouble(etProductPriceDiscount.getText().toString());
                final String brand = etBrand.getText().toString();
                final String storeName = etStoreName.getText().toString();
                final String storeLocation = etStoreLocation.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success) {
                                Intent intent = new Intent(AddItem.this, ItemsUpload.class);
                                AddItem.this.startActivity(intent);
                            }else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddItem.this);
                                builder.setMessage("Upload Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
//                Intent intent = getIntent();
//                Bundle bundle = intent.getExtras();
//                //System.out.println("************"+bundle.getString("id_usuario"));
//                if (bundle != null){
//                    String id_usuario;
//                    id_usuario = bundle.getString("id_usuario");
//                    int fk_usuario = Integer.parseInt(id_usuario);
//                    AddItemRequest addItemRequest = new AddItemRequest(productName, productPrice, productPriceDiscount, brand, storeName, storeLocation, fk_usuario, responseListener);
//                    RequestQueue queue = Volley.newRequestQueue(AddItem.this);
//                    queue.add(addItemRequest);
//                }

                new UploadFileToServer().execute();

                AddItemRequest addItemRequest = new AddItemRequest(productName, image_url, productPrice, productPriceDiscount, brand, storeName, storeLocation, fk_usuario, responseListener);
                RequestQueue queue = Volley.newRequestQueue(AddItem.this);
                queue.add(addItemRequest);

            }
        });

        if (!permissions.checkPermissionForExternalStorage()){
            permissions.requestPermissionForExternalStorage(Permissions.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
        }
    }

    //Mas permisos
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Permissions.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //permission granted successfully

                } else {

                    //permission denied

                }
                break;
        }
    }


    /**
     * Checking device has camera hardware or not
     * */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // successfully captured the image
                // launching upload activity
                System.out.println("CAPTURADA CORRECTAMENTE->" + fileUri.getPath());
                //image_url = fileUri.getPath();
                filePath = fileUri.getPath();
                previewImage(true);


            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        }
    }

    private void previewImage(boolean isImage) {
        if (filePath != null){
            if (isImage){
                imagePreview.setVisibility(View.VISIBLE);
                BitmapFactory.Options options = new BitmapFactory.Options();

                //Image size
                options.inSampleSize = 8;

                final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

                imagePreview.setImageBitmap((bitmap));
            } else {
                imagePreview.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) { return Uri.fromFile(getOutputMediaFile(type)); }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);
        System.out.println("****************"+mediaStorageDir);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");

            image_url = Config.FILE_UPLOAD_FOLDER + "IMG_" + timeStamp + ".jpg";
        } else {
            return null;
        }

        return mediaFile;
    }
//
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FILE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(filePath);
                //File sourceFile = new File(image_url);

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
                //entity.addPart("website",
                  //      new StringBody("www.androidhive.info"));
                //entity.addPart("email", new StringBody("abc@gmail.com"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            //showAlert(result);

            super.onPostExecute(result);
        }

    }
//    private void showAlert(String message) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(message).setTitle("Response from Servers")
//                .setCancelable(false)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // do nothing
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }
}
