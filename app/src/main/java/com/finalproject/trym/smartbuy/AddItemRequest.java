package com.finalproject.trym.smartbuy;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nwni on 11/25/2017.
 */

public class AddItemRequest extends StringRequest{
    //private static final String ADD_ITEM_REQUEST_URL = "http://192.168.1.69/addItem.php";
    private static final String ADD_ITEM_REQUEST_URL = "http://smart-buy.000webhostapp.com/addItem.php";
    private Map<String, String> params;

    public AddItemRequest(String productName, String image_url, double productPrice, double productPriceDiscount, String brand, String storeName,
            String storeLocation, int fk_user, Response.Listener<String> listener) {
        super(Method.POST, ADD_ITEM_REQUEST_URL, listener, null);

        params = new HashMap<>();
        params.put("nombre_producto", productName);
        params.put("ruta_imagen", image_url);
        params.put("precio", String.valueOf(productPrice));
        params.put("precio_descuento", String.valueOf(productPriceDiscount));
        params.put("marca", brand);
        params.put("nombre_tienda", storeName);
        params.put("ubicacion_tienda", storeLocation);
        params.put("fk_usuario", String.valueOf(fk_user));
    }

    @Override
    public Map<String, String> getParams(){
        return params;
    }
}
