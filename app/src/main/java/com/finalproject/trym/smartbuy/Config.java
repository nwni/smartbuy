package com.finalproject.trym.smartbuy;

/**
 * Created by nwni on 11/25/2017.
 */

public class Config {

    //public static final String FILE_UPLOAD_URL = "http://192.168.1.69/AndroidFileUpload/fileUpload.php";
    public static final String FILE_UPLOAD_URL = "http://smart-buy.000webhostapp.com/AndroidFileUpload/fileUpload.php";
    public static final String FILE_UPLOAD_FOLDER = "http://smart-buy.000webhostapp.com/AndroidFileUpload/uploads/";

    public static final String IMAGE_DIRECTORY_NAME = "Items_to_Upload";

}
