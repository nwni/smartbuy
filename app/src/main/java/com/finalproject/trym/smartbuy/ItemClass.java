package com.finalproject.trym.smartbuy;

/**
 * Created by nwni on 11/25/2017.
 */

public class ItemClass {
    private int id_producto;
    private String nombre_producto;
    private String nombre_foto_producto;
    private String ruta_imagen;
    private double precio;
    private double precio_descuento;
    private String marca;
    private String nombre_tienda;
    private String ubicacion_tienda;
    private int fk_usuario;

    public ItemClass(int id_producto, String nombre_producto, String nombre_foto_producto, String ruta_imagen, double precio, double precio_descuento, String marca, String nombre_tienda, String ubicacion_tienda, int fk_usuario) {
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.nombre_foto_producto = nombre_foto_producto;
        this.ruta_imagen = ruta_imagen;
        this.precio = precio;
        this.precio_descuento = precio_descuento;
        this.marca = marca;
        this.nombre_tienda = nombre_tienda;
        this.ubicacion_tienda = ubicacion_tienda;
        this.fk_usuario = fk_usuario;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getNombre_foto_producto() {
        return nombre_foto_producto;
    }

    public void setNombre_foto_producto(String nombre_foto_producto) {
        this.nombre_foto_producto = nombre_foto_producto;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public void setRuta_imagen(String ruta_imagen) {
        this.ruta_imagen = ruta_imagen;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getPrecio_descuento() {
        return precio_descuento;
    }

    public void setPrecio_descuento(double precio_descuento) {
        this.precio_descuento = precio_descuento;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getNombre_tienda() {
        return nombre_tienda;
    }

    public void setNombre_tienda(String nombre_tienda) {
        this.nombre_tienda = nombre_tienda;
    }

    public String getUbicacion_tienda() {
        return ubicacion_tienda;
    }

    public void setUbicacion_tienda(String ubicacion_tienda) {
        this.ubicacion_tienda = ubicacion_tienda;
    }

    public int getFk_usuario() {
        return fk_usuario;
    }

    public void setFk_usuario(int fk_usuario) {
        this.fk_usuario = fk_usuario;
    }
}
