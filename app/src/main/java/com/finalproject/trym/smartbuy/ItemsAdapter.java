package com.finalproject.trym.smartbuy;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by nwni on 11/25/2017.
 */

class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    String itemName;

    private Context context;
    private List<ItemClass> items;

    public ItemsAdapter(Context context, List<ItemClass> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //itemName = items.get(position).getItemName();
        ItemClass item = items.get(position);
        itemName = item.getNombre_producto();
        //holder.itemName.setText(items.get(position).getItemName());
        holder.itemName.setText(item.getNombre_producto());
        holder.itemPrice.setText("Antes $"+items.get(position).getPrecio());
        holder.itemPriceDiscount.setText("Ahora $"+items.get(position).getPrecio_descuento());
        holder.itemStore.setText(items.get(position).getNombre_tienda());
        holder.itemStoreLocation.setText(items.get(position).getUbicacion_tienda());

        //El id de quien subio el item
        //System.out.println("ID-USUARIO->>>"+items.get(position).getFk_usuario());

        //Load the item image using Glide library
        //Hasta ahhora funcina correctamente dando el URL por defecto
        Glide.with(context).load(items.get(position).getRuta_imagen()).into(holder.imageView);
        //Glide.with(context).load("https://via.placeholder.com/300.png").into(holder.imageView);

        //Set a onClickListener to the three dot image
//        holder.overflow.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View view) {
//                //showPopupMenu(holder.overflow);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    //The following allows the user to click on a card and access the menu
    //
    //public  class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{
    public  class ViewHolder extends  RecyclerView.ViewHolder {

        public TextView itemName;
        public TextView itemPrice;
        public TextView itemPriceDiscount;
        public TextView itemStore;
        public TextView itemStoreLocation;
        public ImageView imageView;
        public ImageView overflow;

        public ViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.itemname);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            itemStore = (TextView) itemView.findViewById(R.id.itemstore);
            itemStoreLocation = (TextView) itemView.findViewById(R.id.itemstoreLocation);
            itemPrice = (TextView) itemView.findViewById(R.id.itemprice);
            itemPriceDiscount = (TextView) itemView.findViewById(R.id.itempriceDiscount);
            //overflow = (ImageView) itemView.findViewById(R.id.overflow);

            //imageView.setOnClickListener(this);

        }

//        @Override
//        public void onClick(View v) {
//            int position = getAdapterPosition();
//            showPopupMenu(v,position);
//        }
    }

//    private void showPopupMenu(View view) {
//        PopupMenu popup = new PopupMenu(context, view);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.menu_context, popup.getMenu());
//        popup.setOnMenuItemClickListener(new MenuClickListener());
//        popup.show();
//    }

//    class MenuClickListener implements PopupMenu.OnMenuItemClickListener {
//
//        public MenuClickListener() {
//
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//            switch (menuItem.getItemId()) {
//                case R.id.action_cart:
//                    Toast.makeText(context, itemName + "Added to cart!", Toast.LENGTH_SHORT).show();
//                    return true;
//                case R.id.action_compare:
//                    Toast.makeText(context, itemName + "Added to compare!", Toast.LENGTH_SHORT).show();
//                    return true;
//                default:
//            }
//            return false;
//        }
//    }
}
