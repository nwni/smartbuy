package com.finalproject.trym.smartbuy;

//Funcionalidad para la Vista de los Administradores o Uploaders
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ItemsUpload extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private List<ItemClass> items;
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayout;
    private ItemsAdapter adapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_upload);


        recyclerView=(RecyclerView) findViewById(R.id.recyclerview);
        items = new ArrayList<>();
        getItemsFromDB(0);

        gridLayout = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayout);

        adapter = new ItemsAdapter(this, items);
        recyclerView.setAdapter(adapter);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                Bundle bundle = intent.getExtras();
                //id_usuario = getIntent().getExtras().getString("id_usuario");

                Intent intentAddItem = new Intent(ItemsUpload.this, AddItem.class);
                //System.out.println("************"+bundle.getString("id_usuario"));
                if (bundle != null){
                    String id_usuario;
                    id_usuario = bundle.getString("id_usuario");
                    System.out.println("LO QUE RECIBÍ->>>>>>>>>>"+ id_usuario);

                    intentAddItem.putExtra("id_usuario", id_usuario);
                    //ItemsUpload.this.startActivity(intentAddItem);
                }
                ItemsUpload.this.startActivity(intentAddItem);
            }
        });
    }

    private void getItemsFromDB(int id) {
        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... itemIds) {
                //System.out.println("GETINTEMFROMDB"+items.size());
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        //  .url("http://192.168.1.35/items.php?id=" + itemIds[0])
                        //.url("http://192.168.1.69/items.php?id=" + itemIds[0])
                        .url("http://smart-buy.000webhostapp.com/items.php?id=" + itemIds[0])
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());
                    //System.out.println(array.length());
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        //SI CAMBIO EL SEGUNDO VALOR POR int o double, la app crashea
                        //El valor que llega de la BD es DOUBLE, Lo recibe como string para imprimirlo
//                        ItemClass item = new ItemClass(object.getInt("id"), object.getString("item_price"),
//                                object.getString("item_name"), object.getString("item_image"));
                        //System.out.println("fk_usuario");
                        //System.out.println(object.getInt("fk_usuario"));
                        ItemClass item = new ItemClass(object.getInt("id_producto"), object.getString("nombre_producto"),
                                object.getString("nombre_foto_producto"), object.getString("ruta_imagen"),
                                object.getDouble("precio"), object.getDouble("precio_descuento"),
                                object.getString("marca"), object.getString("nombre_tienda"), object.getString("ubicacion_tienda"),
                                object.getInt("fk_usuario"));

                        ItemsUpload.this.items.add(item);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
            }
        };
        asyncTask.execute(id);
    }
}
