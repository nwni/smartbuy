package com.finalproject.trym.smartbuy;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);

        final Button bLogin = (Button) findViewById(R.id.bLogin);

        final TextView tvRegister = (TextView) findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success) {
                                //PRUEBA
                                //Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                                //LoginActivity.this.startActivity(registerIntent);
                                //PRUEBA
                                //String name = jsonResponse.getString("nombre");
                                int id_usuario = jsonResponse.getInt("id_usuario");
                                int type = jsonResponse.getInt("fk_tipo_usuario");
                                //Tipo de usuario; 1 ADMIN, 2 Uploader, 3 Comun
                                if (type == 1 || type == 2){
                                    //El ID del usuario actual
                                    System.out.println("ESTA MADRE ->>>>>"+id_usuario);
                                    String sendID = Integer.toString(id_usuario);
                                    Intent intentUpload = new Intent(LoginActivity.this, ItemsUpload.class);
                                    intentUpload.putExtra("id_usuario", sendID);
                                    LoginActivity.this.startActivity(intentUpload);
                                }else{
                                    //El ID del usuario actual
                                    System.out.println("ESTA MADRE ->>>>>"+id_usuario);
                                    //Tipo de usuario; 1 ADMIN, 2 Uploader, 3 Comun
                                    //System.out.println("antes del intent: "+ type);
                                    Intent intent = new Intent(LoginActivity.this, Items.class);
                                    //intent.putExtra("nombre", name);
                                    //intent.putExtra("tipo", type);
                                    LoginActivity.this.startActivity(intent);
                                }

                            }else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("Login Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                LoginRequest loginRequest = new LoginRequest(username, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);
            }
        });
    }
}