package com.finalproject.trym.smartbuy;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nwni on 11/24/2017.
 */

public class LoginRequest extends StringRequest{
    //private static final String LOGIN_REQUEST_URL = "http://trym.000webhostapp.com/Login.php";
    //private static final String LOGIN_REQUEST_URL = "http://smartbuy.rf.gd/login.php";
    //private static final String LOGIN_REQUEST_URL = "http://185.27.134.11/login.php";
    //private static final String LOGIN_REQUEST_URL = "http://sql210.epizy.com/login.php";
    private static final String LOGIN_REQUEST_URL = "http://smart-buy.000webhostapp.com/login.php";
    //private static final String LOGIN_REQUEST_URL = "http://192.168.1.69/login.php"; //FUNCIONA LOCAL. Cambiar IP por IP del equipo


    private Map<String, String> params;

    public LoginRequest(String username, String password, Response.Listener<String> listener) {
        super(Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("usuario", username);
        params.put("contrasena", password);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
