package com.finalproject.trym.smartbuy;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nwni on 11/24/2017.
 */

class RegisterRequest extends StringRequest {
    //private static final String REGISTER_REQUEST_URL = "http://192.168.1.69/register.php";
    private static final String REGISTER_REQUEST_URL = "http://smart-buy.000webhostapp.com/register.php";

    private Map<String, String> params;

    public RegisterRequest(String name, String lastName, String username, String email, String password, int fk_tipo_usuario, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("nombre", name);
        params.put("apellidos", lastName);
        params.put("usuario", username);
        params.put("correo", email);
        params.put("contrasena", password);
        params.put("fk_tipo_usuario", Integer.toString(fk_tipo_usuario));
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}