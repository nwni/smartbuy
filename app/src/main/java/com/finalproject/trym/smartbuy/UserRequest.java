package com.finalproject.trym.smartbuy;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by nwni on 11/25/2017.
 */

public class UserRequest extends StringRequest{
    //private static final String USER_REQUEST_URL = "http://192.168.43.105/typeOfUser.php";
    private static final String USER_REQUEST_URL = "http://smart-buy.000webhostapp.com/typeOfUser.php";

    private Map<String, String> params;

    public UserRequest(String username, Response.Listener<String> listener) {
        super(Method.POST, USER_REQUEST_URL, listener, null);
        this.params = params;

        params.put("usuario", username);
    }
    @Override
    public Map<String, String> getParams(){
        return params;
    }
}
